package in.gilsondev.todoapp.resource;

import in.gilsondev.todoapp.entity.Task;
import in.gilsondev.todoapp.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping("/api/tasks")
@Api(value = "Tasks")
@RequiredArgsConstructor
public class TaskResource {
    private final TaskService taskService;

    @ApiOperation(value = "List all tasks")
    @ApiResponse(code = 200, message = "Return all tasks of project" )
    @GetMapping
    public List<Task> listTasks() {
        return taskService.findAll();
    }

    @ApiOperation(value = "Search tasks by description")
    @GetMapping("/search/{query}")
    public List<Task> searchTasks(@PathVariable("query") String query) {
        return taskService.searchTasks(query);
    }

    @ApiOperation(value = "Find task by Identification")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return details of task" ),
            @ApiResponse(code = 404, message = "Task not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity fetchTaskByID(@PathVariable("id") Long taskID) {
        Optional<Task> optionalTask = taskService.findByID(taskID);
        return optionalTask
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @ApiOperation(value = "Create a new task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Task created sucessfully" ),
            @ApiResponse(code = 400, message = "Validation error on Task data")
    })
    @PostMapping
    public ResponseEntity saveTask(@Valid @RequestBody Task taskData) {
        Task task = taskService.save(taskData);

        URI uri = fromCurrentRequest().path("/{id}").buildAndExpand(task.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @ApiOperation(value = "Update task")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Task updated sucessfully" ),
            @ApiResponse(code = 400, message = "Validation error on Task data"),
            @ApiResponse(code = 404, message = "Task not found")
    })
    @PutMapping("/{id}")
    public ResponseEntity updateTask(@PathVariable("id") Long taskID, @Valid @RequestBody Task taskData) {
        Optional<Task> optionalTask = taskService.findByID(taskID);

        if(!optionalTask.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        taskData.setId(taskID);
        taskService.update(taskData);

        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Complete a task")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Task completed sucessfully" ),
            @ApiResponse(code = 400, message = "Validation error on complete the task"),
            @ApiResponse(code = 404, message = "Task not found")
    })
    @PutMapping("/{id}/complete")
    public ResponseEntity completeTask(@PathVariable("id") Long taskID) {
        taskService.completeTask(taskID);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Uncomplete a task")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Task uncompleted sucessfully" ),
            @ApiResponse(code = 400, message = "Validation error on uncomplete the task"),
            @ApiResponse(code = 404, message = "Task not found")
    })
    @PutMapping("/{id}/uncomplete")
    public ResponseEntity uncompleteTask(@PathVariable("id") Long taskID) {
        taskService.uncompleteTask(taskID);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Delete a task by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Task removed sucessfully" ),
            @ApiResponse(code = 404, message = "Task not found")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity deleteTask(@PathVariable("id") Long taskID) {
        Optional<Task> optionalTask = taskService.findByID(taskID);

        return optionalTask
                .map(task -> {
                    taskService.delete(task);
                    return ResponseEntity.noContent().build();
                }).orElseGet(() -> ResponseEntity.notFound().build());
    }
}
