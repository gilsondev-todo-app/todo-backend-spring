package in.gilsondev.todoapp.service;

import in.gilsondev.todoapp.entity.Task;

import java.util.List;
import java.util.Optional;

public interface TaskService {
    List<Task> findAll();

    List<Task> searchTasks(String wordkey);

    Optional<Task> findByID(Long id);

    Task save(Task task);

    Task update(Task task);

    void completeTask(Long id);

    void uncompleteTask(Long id);

    void delete(Task task);
}
