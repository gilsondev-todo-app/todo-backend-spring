package in.gilsondev.todoapp.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Entity
@Table(name = "tasks")
@Data
public class Task {
    @Id
    @ApiModelProperty(notes = "Identity of Task", required = true)
    @SequenceGenerator(name = "taskGenerator", sequenceName = "task_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "taskGenerator", strategy = GenerationType.SEQUENCE)
    private Long id;

    @ApiModelProperty(notes = "Description of Task", required = true)
    @NotBlank(message = "Description is mandatory")
    private String description;

    @ApiModelProperty(notes = "Status of Task", required = true)
    private Boolean done = false;

    @ApiModelProperty(notes = "Date of Task created", required = false)
    @JsonProperty(value = "created_at", required = false)
    private LocalDateTime createdAt = LocalDateTime.now();

    public void completeTask() {
        this.setDone(true);
    }

    public void uncompleteTask() {
        this.setDone(false);
    }
}
